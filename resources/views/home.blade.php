@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <form method="post" class="form-horizontal" action="{{ route('upload') }}" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                        <label>Select file</label>
                        <input type="file" name="file" class="form-control" />
                    </div>

                    <input type="submit" value="submit" class="btn btn-primary"/>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
