@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h3>File details</h3>

                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>Filename</th>
                            <th>File size</th>
                            <th>Extension</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $file_details['filename'] }}</td>
                            <td>{{ $file_details['size'] }} MB</td>
                            <td>{{ $file_details['extension'] }}</td>
                            <td>
                                <select id="cipher">
                                    <option value="aes-128-gcm">aes-128-gcm</option>
                                    <option value="aes-128-cbc">AES-128-CBC</option>
                                    <option value="aes-256-cbc">aes-256-cbc</option>
                                </select>

                                <button id="encrypt" data-file="{{ $file_details['filename'] }}" class="btn btn-info">Encrypt</button>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div id="encrypted-wrapper" style="display: none">
                    <h3>Encrypted file</h3>
                    <table class="table table-condensed">
                        <thead>
                        <tr>
                            <th>Filename</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td id="encrypted-file-path"></td>
                            <td>
                                <button id="decrypt" data-file="" class="btn btn-info">Decrypt</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="original-file-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Original File</h4>
                </div>
                <div class="modal-body">
                    <p></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection


@section('scripts')

    <script>
        $("#encrypt").click(function (e) {
            var filename = $(this).data("file");

            $.ajax({
                url: '{{ route('encrypt') }}',
                data: {filename: filename, cipher: $("#cipher").val()},
                method: 'GET',
                dataType: 'json',
                success: function (response) {
                    $("#encrypted-wrapper").show();

                    $("#encrypted-file-path").html('<a href="'+response.encrypted_file_path+'">'+response.encrypted_file_name+'</a>');

                    $("#decrypt").attr("data-file", response.encrypted_file_name);
                }
            });
        });

        $("#decrypt").click(function (e) {
            var filename = $(this).data("file");

            $.ajax({
                url: '{{ route('decrypt') }}',
                data: {filename: filename},
                method: 'GET',
                dataType: 'json',
                success: function (response) {
                    $("#original-file-modal").find(".modal-body p").html('<a href="'+response.decrypted_file_path+'" target="_blank">Download</a>');

                    $("#original-file-modal").modal("show");
                }
            });
        });
    </script>

@stop