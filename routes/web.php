<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::post('/upload', 'HomeController@upload')->name('upload');

Route::get('/details/{filename}', 'HomeController@details')->name('details');

Route::get('/encrypt', 'HomeController@encrypt')->name('encrypt');

Route::get('/decrypt', 'HomeController@decrypt')->name('decrypt');
