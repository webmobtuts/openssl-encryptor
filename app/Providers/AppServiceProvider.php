<?php

namespace App\Providers;

use App\API\Encryptor;
use App\API\EncryptorInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(EncryptorInterface::class, Encryptor::class);
    }
}
