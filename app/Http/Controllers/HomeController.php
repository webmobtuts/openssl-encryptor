<?php

namespace App\Http\Controllers;

use App\API\EncryptorInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Session;


class HomeController extends Controller
{
    private $encrytor;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(EncryptorInterface $encryptor)
    {
        $this->encrytor = $encryptor;
    }

    /**
     * Show the home page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    /**
     * upload file
     *
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function upload(Request $request)
    {
        try {

            if(!is_writable(public_path('uploads/'))) {

                throw new \Exception("uploads directory not writable! please set writable permissions to this directory");
            }

            if ($request->hasFile('file')) {

                $file = $request->file('file');

                $fileName = $file->getClientOriginalName();

                $file->move(public_path('/uploads'), $fileName);

                @chmod(public_path('/uploads/' . $fileName), 0777);

                return redirect('details/' . $fileName);
            }
        } catch (\Exception $ex) {
            dd($ex->getMessage());
        }
    }


    /**
     * show the file details page
     *
     * @param $filename
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function details($filename)
    {
         $file_details = [
           'filename' => $filename,
           'size' => number_format(filesize(public_path('uploads/' . $filename)) / 1048576, 2),
           'extension' => pathinfo(public_path('uploads/' . $filename), PATHINFO_EXTENSION)
         ];

         return view('details', compact('file_details'));
    }


    /**
     * encrypt
     *
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function encrypt(Request $request)
    {
        $file_contents = file_get_contents(public_path('uploads/' . $request->filename));

        $encrypted = $this->encrytor->encrypt($file_contents, $request->cipher);

        $new_file = pathinfo($request->filename, PATHINFO_FILENAME) . '-encrypted.' . pathinfo($request->filename, PATHINFO_EXTENSION);

        file_put_contents(public_path('uploads/encrypted/' . $new_file), $encrypted);

        @chmod(public_path('uploads/encrypted/' . $new_file), 0777);

        Session::put('ee', [
                  'iv' => $this->encrytor->getIV(),
                  'key' => $this->encrytor->getKey(),
                  'tag' => $this->encrytor->getTag(),
                  'cipher' => $this->encrytor->getCipher()
            ]);

        return response()->json(['encrypted_file_name' => $new_file, 'encrypted_file_path' => url('uploads/encrypted/' . $new_file)]);
    }


    /**
     * decrypt the encrypted file to it's original version
     *
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function decrypt(Request $request)
    {
        $file_contents = file_get_contents(public_path('uploads/encrypted/' . $request->filename));

        $saved_sess = Session::get('ee');

        $this->encrytor->setIV($saved_sess['iv']);
        $this->encrytor->setTag($saved_sess['tag']);
        $this->encrytor->setKey($saved_sess['key']);

        $decrypted = $this->encrytor->decrypt($file_contents, $saved_sess['cipher']);

        $new_file = time().'-'.uniqid() . '.' . pathinfo($request->filename, PATHINFO_EXTENSION);

        file_put_contents(public_path('uploads/decrypted/' . $new_file), $decrypted);

        @chmod(public_path('uploads/decrypted/' . $new_file), 0777);

        return response()->json(['decrypted_file_path' => url('uploads/decrypted/' . $new_file)]);
    }
}
