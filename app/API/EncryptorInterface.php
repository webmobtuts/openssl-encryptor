<?php

namespace App\API;


interface EncryptorInterface
{
    public function encrypt($data, $cipher = null);

    public function decrypt($data, $cipher = null);

    public function setCipher($cipher);

    public function getCipher();

    public function setIV($iv);

    public function getIV();

    public function setTag($tag);

    public function getTag();

    public function setKey($key);

    public function getKey();
}