<?php

namespace App\API;


class Encryptor implements EncryptorInterface
{
    protected $cipher = "aes-128-gcm";

    protected $iv;

    protected $tag;

    protected $key;


    /**
     * encrypt
     *
     *
     * @param $data
     * @param null $cipher
     * @return array
     * @throws \Exception
     */
    public function encrypt($data, $cipher = null)
    {
        if($cipher == null) {
            $cipher = $this->cipher;
        } else {
            $this->cipher = $cipher;
        }

        if(!in_array($cipher, openssl_get_cipher_methods())) {
            throw new \Exception("This cipher is not supported in your current php version");
        }

        $ivlen = openssl_cipher_iv_length($cipher);

        $this->iv = openssl_random_pseudo_bytes($ivlen);

        if($cipher == "aes-128-gcm") {
            $params = [$data, $cipher, $this->key, $options=0, $this->iv, &$this->tag];
        } else {
            $params = [$data, $cipher, $this->key, $options=0, $this->iv];
        }

        $result = call_user_func_array("openssl_encrypt", $params);

        return $result;
    }


    /**
     * decrypt to original data
     *
     * @param $data
     * @param null $cipher
     * @return string
     */
    public function decrypt($data, $cipher = null)
    {
        if($cipher == null) {
            $cipher = $this->cipher;
        } else {
            $this->cipher = $cipher;
        }

        if($cipher == "aes-128-gcm") {
            $params = [$data, $cipher, $this->key, $options=0, $this->iv, &$this->tag];
        } else {
            $params = [$data, $cipher, $this->key, $options=0, $this->iv];
        }


        $result = call_user_func_array("openssl_decrypt", $params);

        return $result;
    }


    /**
     * setCipher
     *
     *
     * @param $cipher
     */
    public function setCipher($cipher)
    {
        $this->cipher = $cipher;
    }


    /**
     * getCipher
     *
     */
    public function getCipher()
    {
        return $this->cipher;
    }


    /**
     * setIV
     *
     *
     * @param $iv
     */
    public function setIV($iv)
    {
        $this->iv = $iv;
    }


    /**
     * getIV
     *
     */
    public function getIV()
    {
        return $this->iv;
    }


    /**
     * setTag
     *
     *
     * @param $tag
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    }


    /**
     * getTag
     *
     */
    public function getTag()
    {
        return $this->tag;
    }


    /**
     * setKey
     *
     *
     * @param $tag
     */
    public function setKey($key)
    {
        $this->key = $key;
    }


    /**
     * getKey
     *
     *
     */
    public function getKey()
    {
        return $this->key;
    }
}